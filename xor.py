from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.optimizers import SGD, Adam
import keras.backend as K

import matplotlib.pyplot as plt
import keras
import numpy as np
import sys


def print_weights(model):
    layer_count = 1
    for layer in model.layers:
        weights = layer.get_weights()
        #  print("RAW:", weights)
        if len(weights) == 0:
            continue
        print("Layer", str(layer_count)+":")
        print("\tConnection Weights:")
        print("\t\t",str(weights[0]).replace('\n', '\n\t\t'))
        print("\tBias Weights:")
        print("\t\t", weights[1])
        layer_count+=1

X = np.array([[0,0],[0,1],[1,0],[1,1]])
y = np.array([[1,0],[0,1],[0,1],[1,0]])
#  X = np.array([[0,1],[1,1], [1,0]]) # missing 0,0
#  y = np.array([[0,1],[1,0], [0,1]])
#  X = np.array([[1,1], [1,0], [0,0]]) # missing 0,1
#  y = np.array([[1,0], [0,1], [1,0]])

model = Sequential()
#  model.add(Dense(4, input_dim=2, kernel_initializer='ones', bias_initializer='zeros'))
model.add(Dense(2, input_dim=2, bias_initializer='ones'))

model.add(Activation('relu'))
#  model.add(Activation('tanh'))
model.add(Dense(2, input_dim=4, bias_initializer='ones'))  
#  model.add(Dense(2, input_dim=2, kernel_initializer='zeros', bias_initializer='zeros'))
#  model.add(Dense(2, input_dim=2))
#  model.add(Dense(2))
#  model.add(Dense(1))
#  model.add(Activation('sigmoid'))
model.add(Activation('softmax'))
#  model.add(Activation('binary'))
#  model.layers[0].set_weights([np.array([[ 3.6347692, -3.480809 ],
#         [-3.6350124,  3.4808996]]), np.array([-2.2966483e-04, -8.4719446e-05])])
#  model.layers[2].set_weights([np.array([[-3.039635 ,  3.3142662],
#         [-3.5558329,  3.0729554]]), np.array([ 6.530701 , -4.5306454])])
model.layers[0].set_weights([np.array([[ -2, 2 ],
       [2,  -2]]), np.array([0, 0])])
model.layers[2].set_weights([np.array([[-2 ,  2],
       [-2,  2]]), np.array([ 0 , 0])])
sgd = SGD(lr=0.07)
#  adam = Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
#  model.compile(loss='binary_crossentropy', optimizer=sgd) # missing 0,0
#  model.compile(loss='mean_squared_error', optimizer=adam)
model.compile(loss='mean_squared_error', optimizer=sgd,  metrics=['accuracy'])
#  model.compile(loss='cosine_proximity', optimizer=sgd)
print_weights(model)
history = model.fit(X, y, batch_size=1, shuffle=True, epochs=int(sys.argv[1]))
print("0,0", model.predict_proba(np.array([[0,0]])))
print("0,1", model.predict_proba(np.array([[0,1]])))
print("1,0", model.predict_proba(np.array([[1,0]])))
print("1,1", model.predict_proba(np.array([[1,1]])))
print_weights(model)
#  plt.plot(history.history['acc'])
#  plt.title('model accuracy')
#  plt.ylabel('accuracy')
#  plt.xlabel('epoch')
#  plt.legend(['train', 'test'], loc='upper left')
#  plt.show()
#  plt.plot(history.history['loss'])
#  plt.title('model loss')
#  plt.ylabel('loss')
#  plt.xlabel('epoch')
#  plt.legend(['train', 'test'], loc='upper left')
#  plt.show()

